
package Main;

import view.FPadre;

public class Main {
    
    public static void main(String[] args) {
        FPadre n = new FPadre();
        n.setVisible(true);
        n.setLocationRelativeTo(null);
        n.setResizable(false);
    }
    
}

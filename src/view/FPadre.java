
package view;

public class FPadre extends javax.swing.JFrame {

    /**
     * Creates new form FPadre
     */
    public FPadre() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        JButton_Calculos1 = new javax.swing.JButton();
        JButton_Salir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Programa para calculos matematicos");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        JButton_Calculos1.setBackground(new java.awt.Color(102, 102, 255));
        JButton_Calculos1.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        JButton_Calculos1.setForeground(new java.awt.Color(51, 255, 51));
        JButton_Calculos1.setText("Calculos");
        JButton_Calculos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JButton_Calculos1ActionPerformed(evt);
            }
        });
        getContentPane().add(JButton_Calculos1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 30, 130, 50));

        JButton_Salir.setBackground(new java.awt.Color(102, 102, 255));
        JButton_Salir.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        JButton_Salir.setForeground(new java.awt.Color(51, 255, 51));
        JButton_Salir.setText("Salir");
        JButton_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JButton_SalirActionPerformed(evt);
            }
        });
        getContentPane().add(JButton_Salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 130, 50));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/imagenes/images2.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 450));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JButton_SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JButton_SalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_JButton_SalirActionPerformed

    private void JButton_Calculos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JButton_Calculos1ActionPerformed
        FCalculos c = new FCalculos();
        c.setVisible(true);
        c.setLocationRelativeTo(null);
        c.setResizable(false);
    }//GEN-LAST:event_JButton_Calculos1ActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JButton_Calculos1;
    private javax.swing.JButton JButton_Salir;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}

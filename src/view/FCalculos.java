package view;

import core.OnlyDigits;
import core.Proceso;
import java.awt.Color;
import java.awt.Font;

public class FCalculos extends javax.swing.JFrame {

    private static final long serialVersionUID = 1L;
    
    public FCalculos() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1_Numero = new javax.swing.JLabel();
        jTextField1_numero = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jButton_Primo = new javax.swing.JButton();
        jButton3_factorial = new javax.swing.JButton();
        jButton1_Nfibonacci = new javax.swing.JButton();
        jTextField2_Resultado = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jButton2_FIBONACCI = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1_Fibo = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1_Numero.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1_Numero.setText("Ingresa un numero");

        jTextField1_numero.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextField1_numero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1_numeroActionPerformed(evt);
            }
        });
        jTextField1_numero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1_numeroKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1_numeroKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1_Numero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField1_numero, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(218, 218, 218))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1_Numero)
                    .addComponent(jTextField1_numero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(102, 102, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton_Primo.setText("Es Primo ?");
        jButton_Primo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jButton_Primo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_PrimoActionPerformed(evt);
            }
        });
        jPanel2.add(jButton_Primo, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 120, 30));

        jButton3_factorial.setText("factorial");
        jButton3_factorial.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jButton3_factorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3_factorialActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3_factorial, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 0, 120, 30));

        jButton1_Nfibonacci.setText("n-fibonacci");
        jButton1_Nfibonacci.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jButton1_Nfibonacci.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1_NfibonacciActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1_Nfibonacci, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 140, 30));

        jTextField2_Resultado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextField2_Resultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2_ResultadoActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField2_Resultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 187, -1));

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setText("        Resultado");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 168, 20));

        jButton2_FIBONACCI.setText("Desplegar fibonacci");
        jButton2_FIBONACCI.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jButton2_FIBONACCI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2_FIBONACCIActionPerformed(evt);
            }
        });

        jTextArea1_Fibo.setColumns(20);
        jTextArea1_Fibo.setRows(5);
        jTextArea1_Fibo.setEnabled(false);
        jScrollPane1.setViewportView(jTextArea1_Fibo);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2_FIBONACCI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2_FIBONACCI, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1_numeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1_numeroActionPerformed

    }//GEN-LAST:event_jTextField1_numeroActionPerformed

    private void jButton_PrimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_PrimoActionPerformed
        if (llenado(jTextField1_numero.getText())) {
            int conver = Integer.parseInt(jTextField1_numero.getText());
            boolean retorno = Proceso.primo(conver);
            jTextField2_Resultado.setForeground(Color.BLACK);
            jTextField2_Resultado.setText(retorno ? "ES PRIMO" : "NO ES PRIMO");
        } else {
            jTextField2_Resultado.setFont(new Font("Serif",Font.PLAIN,18));
            jTextField2_Resultado.setForeground(Color.red);
            jTextField2_Resultado.setText("Ingresa un numero");
        }
    }//GEN-LAST:event_jButton_PrimoActionPerformed

    private void jTextField1_numeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1_numeroKeyTyped

    }//GEN-LAST:event_jTextField1_numeroKeyTyped

    private void jTextField1_numeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1_numeroKeyPressed
        jTextField1_numero.addKeyListener(new OnlyDigits());
    }//GEN-LAST:event_jTextField1_numeroKeyPressed

    private void jButton3_factorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3_factorialActionPerformed

        if (llenado(jTextField1_numero.getText())) {
            int conver = Integer.parseInt(jTextField1_numero.getText());
            double resultado = Proceso.factorial(conver);
            jTextField2_Resultado.setForeground(Color.BLACK);
            jTextField2_Resultado.setText("" + resultado);
        } else {
            jTextField2_Resultado.setFont(new Font("Serif",Font.PLAIN,18));
            jTextField2_Resultado.setForeground(Color.red);
            jTextField2_Resultado.setText("Ingresa un numero");
        }
    }//GEN-LAST:event_jButton3_factorialActionPerformed

    private void jButton1_NfibonacciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1_NfibonacciActionPerformed
        if (llenado(jTextField1_numero.getText())) {
            int conver = Integer.parseInt(jTextField1_numero.getText());
            long resultado = Proceso.Nesimofibonacci(conver);
            jTextField2_Resultado.setForeground(Color.BLACK);
            jTextField2_Resultado.setText("" + resultado);
        } else {
            jTextField2_Resultado.setFont(new Font("Serif",Font.PLAIN,18));
            jTextField2_Resultado.setForeground(Color.red);
            jTextField2_Resultado.setText("Ingresa un numero");
        }


    }//GEN-LAST:event_jButton1_NfibonacciActionPerformed

    private void jTextField2_ResultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2_ResultadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2_ResultadoActionPerformed

    private void jButton2_FIBONACCIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2_FIBONACCIActionPerformed
        if (llenado(jTextField1_numero.getText())) {
            
            int conver = Integer.parseInt(jTextField1_numero.getText());
            jTextField2_Resultado.setForeground(Color.BLACK);
            switch (conver) {
            case 1: {
                jTextArea1_Fibo.setText("[ 1 ] = 1");
            }
            break;
            default: {

                System.out.println("\t\t [ 1 ] = 1");
                for (double p = (long) 2, i = (long) 0, j = (long) 1; p <= conver; ++p, j = i + j) {
                    jTextArea1_Fibo.setText(String.format(" [ %.0f ] =  %.0f%n", p, j));
                    i = j - i;
                }
            }
        }  
        } else {
            jTextField2_Resultado.setFont(new Font("Serif",Font.PLAIN,18));
            jTextField2_Resultado.setForeground(Color.red);
            jTextField2_Resultado.setText("Ingresa un numero");
        }
    }//GEN-LAST:event_jButton2_FIBONACCIActionPerformed

    private boolean llenado(String ll) {
        try {
            Integer.parseInt(ll);
            return true;
        } catch (NumberFormatException n) {
            return false;
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1_Nfibonacci;
    private javax.swing.JButton jButton2_FIBONACCI;
    private javax.swing.JButton jButton3_factorial;
    private javax.swing.JButton jButton_Primo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel1_Numero;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1_Fibo;
    private javax.swing.JTextField jTextField1_numero;
    private javax.swing.JTextField jTextField2_Resultado;
    // End of variables declaration//GEN-END:variables
}

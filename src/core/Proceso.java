package core;

public class Proceso {

    @SuppressWarnings("fallthrough")
    public static boolean primo(int n) {
        boolean bandera = false;
        switch (n) {
            case 0:
                bandera = false;
                break;
            case 1:
            case 2:
                bandera = true;
                break;
            default:
                for (int i = 2; i < Math.sqrt(n); i++) 
                {
                    if (n % i == 0) {
                        bandera = false;
                    }
                }   break;
        }
        return bandera;
    }// Fin del metodo primo

    public static double factorial(int numero) {
        switch (numero) {
            case 0:
            case 1:
                return 1;
            default: {
                double retorno = numero;
                for (int i = (numero - 1); i >= 1; i--) {
                    retorno = (retorno) * (i);
                }
                return retorno;
            }

        }
    }

    public static long Nesimofibonacci(int n) {
        long s = 1, u = 1, p = 1;
        int i;
        if (n == 0) {
            s = 0;
        } else {
            for (i = 3; i <= n; i++) {
                p = u;
                u = s;
                s += p;
            }
        }
        return s;

    }

    public void fibonacci(int numero) {
        switch (numero) {
            case 1: {
                System.out.println(" [ 1 ] =  1");
            }
            break;
            default: {

                System.out.println(" [ 1 ] = 1");
                for (double p = (long) 2, i = (long) 0, j = (long) 1; p <= numero; ++p, j = i + j) {
                    System.out.printf(" [ %.0f ] =  %.0f%n", p, j);
                    i = j - i;
                }
            }
        }

    }

}
